import { useState } from "react";


const CounterSizeGenerator = (props) => {
    const { size, setSize } = props;


    const updateSize = (e) => {
        setSize(e.target.value);
        console.log(size);
    }
    return (
        <div>
            Size:<input type='number' value={size || ''} onChange={updateSize}></input>
        </div>
    );

};

export default CounterSizeGenerator;