import Counter from "./Counter";

const CounterGroup = (props) => {
    const { counterList, setCounterList } = props;

    const updateCounterValue = (newCountValue, index) => {
        console.log(newCountValue);
        const list = counterList.map((currentNumber, itemIndex) => {
            if (itemIndex === index) {
                return newCountValue;
            }
            return currentNumber;
        })
        setCounterList(list);
    }
    return (
        <div>
            {counterList.map((counter, index) => {
                return <Counter
                    key={index}
                    counter={counter}
                    updateCounterValue={(counter) => updateCounterValue(counter, index)} />;
            })}
        </div>
    )
};
export default CounterGroup;